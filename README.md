Unity3D project which was created during the course "Advanced Interactive Systems". 

The application allows to track a person via kinect and map it to a virtual model of a robot. The movements can be recorded and replayed. 

The application can be controlled either via keyboard/mouse or via voice commands.

**The project was part of our studies in Computer Science (M.Sc.).**