﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Kinect;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

/// <summary>
/// Diese Klasse repräsentiert die Datenstruktur der AvatarMotions (Normale und Idle Motions).
/// und beinhaltet entsprechende Getter, Setter und Methoden.
/// </summary>
[System.Serializable]
public class AvatarMotion 
{
	/// <summary>
	/// Global verfügbare Liste von exisitierenden Motions.
	/// Wird beim Laden der Motions und beim Aufzeichnen von Motions befüllt.
	/// Enthält zu jedem Zeitpunkt alle verfügbaren Motions.
	/// </summary>
	public static List<AvatarMotion> Motions = new List<AvatarMotion>();
	
	/// <summary>
	/// Global verfügbare Liste von exisitierenden Idle-Motions.
	/// Wird beim Laden der Idle-Motions befüllt (Also beim Programmstart).
	/// Enthält zu jedem Zeitpunkt alle verfügbaren Idle-Motions.
	/// </summary>
	public static List<AvatarMotion> IdleMotions = new List<AvatarMotion>();
	
	/// <summary>
	/// Name der Motion. Änderbar durch den Benutzer.
	/// </summary>
	private string mName;
	
	/// <summary>
	/// Die Liste der Skelettdaten einer Motion. Enthält einen Skeletdatensatz pro aufgezeichnetem Frame.
	/// </summary>
	private List<SerialSkeletonFrame> mMotionFrames;
	
	/// <summary>
	/// Zeitpunkt zu dem die Motion aufgezeichnet bzw. zuletzt geändert wurde.
	/// </summary>
	private string mTimestamp;
	
	/// <summary>
	/// Der absolute Dateipfad zur gespeicherten Motion Datei (.mot oder .idle).
	/// </summary>
	private string mAbsFilepath;
	
	/// <summary>
	/// Konstruktor der Klasse. 
	/// Initialisiert Attribute der zu erzeugenden Instanz (außer Dateipfad).
	/// </summary>
	public AvatarMotion ()
	{
		setName("Motion "+Motions.Count);
		setMotionFrames( new List<SerialSkeletonFrame>() );
		setTimestamp(System.DateTime.Now.ToString("yyyyMMddHHmmssffff"));
	}
	
	//*****GETTERS AND SETTERS******//
	public string getName()
	{
		return mName;
	}
	
	public ReadOnlyCollection<SerialSkeletonFrame> getMotionFrames()
	{
		return mMotionFrames.AsReadOnly();
	}
	
	public string getTimestamp()
	{
		return mTimestamp;
	}
	
	public string getAbsFilePath()
	{
		return mAbsFilepath;
	}
	
	public void setName(string name)
	{
		mName = name;
	}
	
	private void setMotionFrames(List<SerialSkeletonFrame> skeletonFrames)
	{
		mMotionFrames = skeletonFrames;
	}
	
	private void setTimestamp(string ts)
	{
		mTimestamp = ts;
	}
	
	public void setAbsFilepath(string path)
	{
		mAbsFilepath = path;
	}
	
	//******METHODS*******//
	/// <summary>
	/// Fügt einen Skelettdatensatz zur FrameListe hinzu.
	/// Sollte während der Aufzeichnung ein Mal pro Frame aufgerufen werden.
	/// </summary>
	/// <param name='skeletonFrame'>
	/// Der Skelettdatensatz der hinzugefügt werden soll.
	/// </param>
	public void addMotionFrame(SerialSkeletonFrame skeletonFrame)
	{
		mMotionFrames.Add(skeletonFrame);
	}
	
	/// <summary>
	/// Serialisiert die Instanz der Klasse und speichert sie in einer Datei unter dem angegebenen absoluten Dateipfad ab.
	/// </summary>
	/// <param name='absPath'>
	/// Der absolute Dateipfad unter dem die Motion abgespeichert werden soll.
	/// </param>
	public void writeToFile(string absPath)
	{
		if(File.Exists(absPath))
			File.Delete(absPath);
		
		if(File.Exists(mAbsFilepath))
			File.Delete(mAbsFilepath);
		
		setAbsFilepath(absPath);
	
		FileStream output = new FileStream(absPath, FileMode.Create);

		BinaryFormatter bf = new BinaryFormatter();
		bf.Serialize(output, this);
		output.Close();
	}
}
