﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Customer webcam
/// Lädt alle Webcams, die an dem Computer angeschlossen sind in eine liste
/// Gehört zur "Operator-Szene" und wird im GameObject "CamCustomer" eingesetzt
/// Wird ebenfalls in der Klasse GUIOperator benutzt
/// </summary>
public class CustomerWebcam : MonoBehaviour {
	
	private WebCamDevice[] devices; 	
	private string [] sDevices;
	private	WebCamTexture webcam;
	
	/// <summary>
	/// Wird beim Laden des zugehörigen GameObjects automatisch einmal aufgerufen
	/// Initialisiert alle Webcams, die an den PC angeschlossen sind
	/// Falls eine Webcam keinen Namen besitzt, bekommt sie den Namen untitled
	/// Nachdem alle Webcams geladen wurden, wird die erste als Standard in die Operator-Szene geladen
	/// </summary>
	void Start () {
		devices = WebCamTexture.devices;
		sDevices = new string[devices.Length];
		for(int i = 0; i<devices.Length; i++)
		{
			if(devices[i].name.Equals(""))
				sDevices[i] = "untitled";
			else
				sDevices[i] = devices[i].name;
		}
		if (devices.Length <= 0)
			return;
		
		webcam = new WebCamTexture(devices[0].name);
		renderer.material.mainTexture = webcam;
		webcam.Play();
	}
	
	/// <summary>
	/// Gibt die Namen aller angeschlossenen Webcams zurück
	/// </summary>
	/// <returns>
	/// string array mit allen WebCam Namen
	/// </returns>
	public string [] GetDevices()
	{
		return sDevices;
	}
	
	/// <summary>
	///  Gibt die Anzahl der am PC angeschlossenen Webcams zurück
	/// </summary>
	/// <returns>
	/// integer mit der Anzahl der am PC angeschlossenen Webcams
	/// </returns>
	public int getNumberOfWebcams()
	{
		return sDevices.Length;
	}
	
	/// <summary>
	/// Stellt die Anzeige in der Operator-Szene anhand eines integers auf eine neue Webcam um
	/// </summary>
	/// <param name='index'>
	/// index wird zuerst geprüft >> falls dieser existiert wird das Bild der Kamera in die GUI übernommen
	/// </param>
	public void SetCustomerWebcam(int index)
	{	
		if (index >= 0 && index < sDevices.Length)
		{
			webcam.Stop();
		 	webcam = new WebCamTexture(sDevices[index]);
			renderer.material.mainTexture = webcam;
			webcam.Play();
		}		
	}
	/// <summary>
	/// Stellt die Anzeige in der Operator-Szene anhand eines strings auf eine neue Webcam um
	/// </summary>
	/// <param name='name'>
	/// Das Bild einer neuen Kamera wird in die GUI übernommen
	/// </param>
	public void SetCustomerWebcam(string name)
	{	
		webcam.Stop();
		webcam = new WebCamTexture(name);
		renderer.material.mainTexture = webcam;
		webcam.Play();	
	}
	/// <summary>
	/// Stopt die Webcam - Wird beim Beenden der Operator Szene aufgerufen
	/// </summary>
	public void StopWebcam()
	{
		webcam.Stop();	
	}

}
