﻿using UnityEngine;
using System.Collections;
/// <summary>
/// ShopAvatarAudioClient.
/// Setzt und steuert das Mikrofon im Operator Modus
/// Gehört zur "Operator-Szene" und wird im GameObject "AudioClient" eingesetzt
/// Wird ebenfalls in der Klasse GUIOperator benutzt
/// </summary>
public class ShopAvatarAudioClient : MonoBehaviour {
	
	private string micro;
	private AudioSource aud;
	private string[] devices;
	
	/// <summary>	
	/// Wird beim Laden des zugehörigen GameObjects automatisch einmal aufgerufen
	/// Holt sich alle Audio-Inputs
	/// Wenn Audio-Inputs vorhanden sind, werden diese alle in das string array devices geschrieben 
	/// und das erste Mikrofon benutzt
	/// </summary>
	void Start () {
		aud = GetComponent<AudioSource>();
		if (null != aud) 
		{
			devices = Microphone.devices;
			for(int i = 0; i<devices.Length; i++)
			{
				if(devices[i].Equals(""))
					devices[i] = "untitled";
			}
			
			if(devices.Length > 0)
				SetMicrophone(devices[0]);
		}
	}

	/// <summary>
	/// Anzahl der Mikrofone zurückgeben
	/// </summary>
	/// <returns>
	/// Gibt die Anzahl der Mikrofone als integer zurück
	/// </returns>
	public int getNumberOfMicrophones()
	{
		return devices.Length;
	}
	
	/// <summary>
	/// Alle namen der Mikrofone zurückgeben
	/// </summary>
	/// <returns>
	/// Gibt alle namen der Mikrofone in einem string array zurück
	/// </returns>
	public string[] GetDevices()
	{
		return devices;
	}
	
	/// <summary>
	/// Nimmt ein neues Mikrofon für den Operator Mode
	/// </summary>
	/// <param name='src'>
	/// Setzt ein neues Mikrofon anhand des namens
	/// </param>
	public void SetMicrophone(string src)
	{	
		if (null != aud) 
		{
			micro = src;
			audio.loop = true;
			aud.clip = Microphone.Start(micro, true, 10, 44100); 
			while (!(Microphone.GetPosition(micro) > 0)){} // Wait until the recording has started
				audio.Play(); // Play the audio source!
		}
	}
	
	/// <summary>
	/// Schaltet das Mikrofone auf Stumm und zurück
	/// </summary>
	/// <param name='mute'>
	/// true = mute
	/// false = unmute
	/// </param>
	public void SetMicrophoneMute(bool mute)
	{
		audio.mute = mute;
	}
}
