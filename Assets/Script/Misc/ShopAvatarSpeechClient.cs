﻿
using UnityEngine;
using System.Collections;
using System;
using System.Net;
using System.Text;
using System.Net.Sockets;
using System.Threading;


/// <summary>
/// ShopAvatarSpeechClient
/// Empfängt Daten von dem in Visual Studio gebauten "ShopAvatarSpeechServer" welcher strings via UDP-Verbindung über die lokale IP Adresse 127.0.0.1:42 überträgt
/// Gehört zur "Recording-Szene" und wird im GameObject "Speech" eingesetzt
/// Wird ebenfalls in der Klasse GUIRecorder benutzt
/// </summary>
public class ShopAvatarSpeechClient : MonoBehaviour {
		
	private MotionRecorder recorder;

	private Thread receiveThread;
	private UdpClient client;
	private int port; 
	private string strReceiveText;

	/// <summary>
	///  Wird beim Laden des zugehörigen GameObjects automatisch einmal aufgerufen
	///	Setzt den UDP-Port und eröffnet einen neuen Thread
	/// </summary>
	void Start()
	{
		strReceiveText = "";
		port = 42;
		recorder = KinectInstance.Instance.recorder;
		receiveThread = new Thread( new ThreadStart(ReceiveData));
		if(receiveThread != null)
		{
			receiveThread.IsBackground = true;
			receiveThread.Start();
		}
	}
	
	/// <summary>
	/// UDPs the get packet.
	/// </summary>
	/// <returns>
	/// The get packet.
	/// </returns>
	public string UDPGetPacket()
	{
		return strReceiveText;
	}
	
	/// <summary>
	/// Wenn auf GameObjekt auf inaktiv wechselt wird der Thread abgebrochen und der Client geschlossen
	/// </summary>
	void OnDisable()
	{
		if ( receiveThread != null) receiveThread.Abort();
		if(client != null) client.Close();
	}
	
	/// <summary>
	/// Empfängt die Daten vom in Visual Studio entwickelten SpeechServer
	/// Ruft für verschiedene Eingaben verschiedene Methoden auf 
	/// </summary>
	private  void ReceiveData()
	{
		client = new UdpClient(port);
		while (true)
		{
			try
			{
				byte[] data;
				IPEndPoint anyIP = new IPEndPoint(IPAddress.Broadcast, port);
				data = client.Receive(ref anyIP);
				strReceiveText = Encoding.UTF8.GetString(data);
				switch (strReceiveText){
					case "start":
						recorder.StartRecording();
						break;
					case "end":
						recorder.StopRecording();
						break;
				}
			}
			catch (Exception err)
			{
				Debug.Log(err.ToString());
			}
		}
	}
}
    // *********************************************************
