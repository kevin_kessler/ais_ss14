using UnityEngine;
using System.Collections;
using System.IO;
using System.Diagnostics;
/// <summary>
/// Startet den in Visual Studio gebauten "ShopAvatarSpeechServer" welcher strings via UDP-Verbindung über die lokale IP Adresse 127.0.0.1:42 überträgt
/// Gehört zur "Recording-Szene" und wird im GameObject "Speech" eingesetzt
/// Wird ebenfalls in der Klasse GUIRecorder benutzt
/// </summary>
public class ShopAvatarStartServer : MonoBehaviour {
	
	private Process SpeechServerProcess = new Process();
	
	/// <summary>
	///  Wird beim Laden des zugehörigen GameObjects automatisch einmal aufgerufen
	///	 Erstellt einen neuen Prozess, welche den SpeechServer startet. Dieser sendet strings an den Speech Client
	/// </summary>
	void Start () {
        try {
	        string path = Application.dataPath + @"\SpeechServer\ShopAvatarSpeechServer.exe";
			SpeechServerProcess.StartInfo.FileName = path;
	        SpeechServerProcess.EnableRaisingEvents = true;
	        //Start the Server
			SpeechServerProcess.Start();
        } catch (System.Exception e){
			SpeechServerProcess = null;
            UnityEngine.Debug.Log(e);       
        }
	}
	/// <summary>
	/// Wird ausgelöst bevor die Applikation beendet wird.
	/// Schließt das externe Programm "ShopAvatarSpeechServer"
	/// </summary>
	void OnApplicationQuit()
	{
		CloseSpeechServer();
	}
	
	/// <summary>
	/// Beendet den Speech Server
	/// </summary>
	public void CloseSpeechServer() {		
		if(null != SpeechServerProcess && !SpeechServerProcess.HasExited)
			SpeechServerProcess.Kill();
	}
}
