﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Diese Klasse wird benötigt um die Anwendung und ihre GUI-Elemente unabhängig von der Auflösung des Endgeräts zu machen.
/// Sie stellt Funktionen bereit die zum Anpassen der GUI-Rechtecke auf die aktuelle Auflösung verwendet werden.
/// </summary>
public class Resize {
	
	/// <summary>
	/// Fensterbreite auf der die Berechnungen der Rechtecke für das GUI des Operatorbildschirms basiert.
	/// Operatorbildschirm = Linker Teil des Recording Modus und des Operatormodus und Hauptmenü.
	/// </summary>
	public const float ORIG_SCREENWIDTH = 1024f;
	
	/// <summary>
	/// Fensterhöhe auf der die Berechnungen der Rechtecke für das GUI des Operatorbildschirms basiert.
	/// Operatorbildschirm = Linker Teil des Recording Modus und des Operatormodus und Hauptmenü.
	/// </summary>
	public const float ORIG_SCREENHEIGHT = 768f;
	
	/// <summary>
	/// Berechnet ein auf die eigentliche Auflösung angepasstes Rechteck im Verhältnis zu der Auflösung 1024x786.
	/// </summary>
	/// <returns>
	/// Das für die eigentliche Auflösung angepasste Rechteck.
	/// </returns>
	/// <param name='rect'>
	/// Das Rechteck dass auf der Auflösung 1024x786 basiert.
	/// </param>
	public static Rect ResizeRectOperatorScreen(Rect rect)
	{
	    float widthFraction = rect.width / ORIG_SCREENWIDTH;
		float heightFraction = rect.height / ORIG_SCREENHEIGHT;
		float screenwidth = Screen.width/28*12;
	    float width = widthFraction * screenwidth;
	    float height = heightFraction * Screen.height;
		
	    float x = (rect.x / ORIG_SCREENWIDTH) * screenwidth;
	    float y = (rect.y / ORIG_SCREENHEIGHT) * Screen.height;
	 
	    return new Rect(x,y,width,height);
	}
}
