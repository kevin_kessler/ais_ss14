﻿using UnityEngine;
using System.Collections;

/// <summary>
/// GUI Recorder. Script zur Darstellung der GUI-Elemente der "Recorder-Szene"
/// Wird in dem GameObject "GUI" in der Recorder-Szene verwendet
/// </summary>
public class GUIRecorder : MonoBehaviour {
	
	private MotionPlayer player;
	private MotionRecorder recorder;
	
	/// <summary>
	/// Die momentan ausgewählte AvatarMotion (zum abspielen, bearbeiten oder löschen).
	/// </summary>
	private AvatarMotion selectedMotion = null;
	
	/// <summary>
	/// Die GUI Plane die das REC Symbol darstellt wenn aufgenommen wird.
	/// Muss im Unity Inspector zugewiesen werden.
	/// </summary>
	public GameObject recPlane;
	
	/// <summary>
	/// Komponente zum Starten der externen SpeechServer.exe um Sprachkommandos entgegenenehmen zu können
	/// </summary>
	public ShopAvatarStartServer speechServer;
	
	
	/******* Variablen die die Postition, Größe und das Aussehen der GUI-Elemente beschreiben ********/
	private Rect motionRect;
	private Rect motionScrollRect;
	private Vector2 motionScrollPos;
	private Rect motionManageArea;
	private Rect motionManageButton;
	private int motionManageButtonNumber;
	
	private bool stylesSet = false;
	private GUIStyle selButtonStyle;
	private GUIStyle normalButtonStyle;
	private GUIStyle textFieldStyle;
	
	private bool renaming = false;
	private Rect renameArea;
	private Rect renameOKRect;
	private Rect renameCancelRect;
	private Rect renameTextRect;
	private string renameString;
	
	private Rect exitRect;
	
	private Rect sensorAngleArea;
	private Rect sensorUpRect;
	private Rect sensorDownRect;
	private Rect sensorValRect;
	private int sensorAngleVal = 0;
	
	/// <summary>
	/// Wird beim Laden des zugehörigen GameObjects automatisch ein Mal aufgerufen.
	/// Dient zur Initialisierung der Klasse und ihrer Attribute.
	/// </summary>
	void Start () {
		player = KinectInstance.Instance.player;
		recorder = KinectInstance.Instance.recorder;
		calcRects();
	}
	
	/// <summary>
	/// Berechnet die Größe und Position der Rechtecke der GUI-Elemente
	/// </summary>
	private void calcRects()
	{
		motionScrollRect = new Rect(25, 250, 315, Resize.ORIG_SCREENHEIGHT - 355);
		motionScrollPos = Vector2.zero;
		motionRect = new Rect(0,0,motionScrollRect.width-28,50);
		motionManageArea = new Rect(motionScrollRect.xMin, motionScrollRect.yMax +15, motionScrollRect.width, 70);
		
		motionManageButtonNumber = 3;
		motionManageButton = new Rect(0,0,motionManageArea.width/motionManageButtonNumber, motionManageArea.height);
		
		renameArea = new Rect(Resize.ORIG_SCREENWIDTH/2 - Resize.ORIG_SCREENWIDTH/4, Resize.ORIG_SCREENHEIGHT/2 - Resize.ORIG_SCREENHEIGHT/8, Resize.ORIG_SCREENWIDTH/2, Resize.ORIG_SCREENHEIGHT/4); 
		renameTextRect = new Rect(renameArea.width/2 - renameArea.width/4, renameArea.height/2 - 50, renameArea.width/2, 50);
		renameOKRect = new Rect(renameArea.width - renameArea.width/3 - 10, renameArea.height - 50 - 10, renameArea.width/3, 50);
		renameCancelRect = new Rect(10, renameOKRect.yMin, renameOKRect.width, renameOKRect.height);
		
		exitRect = new Rect(844, Resize.ORIG_SCREENHEIGHT - 90,150,70);
	
		
		sensorAngleArea = new Rect(320, 25, 60, 155);
		sensorUpRect = new Rect(0,0, 60, 50);
		sensorValRect = new Rect(0, sensorUpRect.yMax+5, sensorUpRect.width, 30);
		sensorDownRect = new Rect(0, sensorValRect.yMax+5, sensorUpRect.width, sensorUpRect.height);
	}
	
	/// <summary>
	/// Initialisiert die verschiedenen GUI-Styles die zur Darstellung der GUI-Elemente benötigt werden.
	/// </summary>
	private void setStyles()
	{
		normalButtonStyle =  GUI.skin.button;
		
		selButtonStyle = new GUIStyle(normalButtonStyle);
		selButtonStyle.normal.textColor = Color.red;
		selButtonStyle.hover.textColor = Color.red;
		
		textFieldStyle = new GUIStyle( GUI.skin.textField );
		textFieldStyle.alignment = TextAnchor.MiddleCenter;
	}
	
	/// <summary>
	/// GameLoop, wird ein Mal pro Frame aufgerufen.
	/// Aktiviert bzw. deaktiviert die GUI-Plane für das REC Symbol, je nach Aufzeichnungsstatus
	/// </summary>
	void Update () 
	{		
		if(recorder.IsRecording() && !recPlane.activeInHierarchy)
			recPlane.SetActive(true);
		else if(!recorder.IsRecording() && recPlane.activeInHierarchy)
			recPlane.SetActive(false);
	}
	
	/// <summary>
	/// Zuständig für das Zeichnen von GUI-Elementen. Wird mindestens ein Mal pro Frame aufgerufen.
	/// Zeichnet die einzelnen GUI-Bereiche des Recorder Modes.
	/// Dazu zählen Buttons für den Sensorwinkel, die Liste der Motions, die Buttons zum Bearbeiten der Motions
	/// und das Renaming Pop-Up.
	/// </summary>
	void OnGUI()
	{
		if(!stylesSet)
			setStyles();
		
		GUI.enabled = !renaming;
		
		Rect tmpExitRect = Resize.ResizeRectOperatorScreen(exitRect);
		if(GUI.Button(tmpExitRect, "Exit")){
			speechServer.CloseSpeechServer();
			if(recorder.IsRecording())
				recorder.StopRecording();
			Application.LoadLevel("MainMenu");
		}
		
		drawSensorAngleArea();
		drawMotionArea();

		GUI.enabled = true;
		
		if(renaming)
			drawRenamePopUp();
	}
	
	/// <summary>
	/// Zeichnet den Sensorwinkel GUI-Bereich
	/// </summary>
	private void drawSensorAngleArea()
	{
		Rect tmpUp = Resize.ResizeRectOperatorScreen(sensorUpRect);
		Rect tmpVal = Resize.ResizeRectOperatorScreen(sensorValRect);
		Rect tmpDown = Resize.ResizeRectOperatorScreen(sensorDownRect);
		Rect tmpSensorAngleArea = Resize.ResizeRectOperatorScreen(sensorAngleArea);
		
		GUI.BeginGroup(tmpSensorAngleArea);
		
		if(GUI.Button(tmpUp, "Up"))
		{
			if(sensorAngleVal < 27)
				KinectInstance.Instance.setSensorAngle(++sensorAngleVal);
		}
		
		GUI.Box(tmpVal, ""+sensorAngleVal);
		
		if(GUI.Button(tmpDown, "Down"))
		{
			if(sensorAngleVal > -27)
				KinectInstance.Instance.setSensorAngle(--sensorAngleVal);
		}
		GUI.EndGroup();
	}
	
	/// <summary>
	/// Zeichnet den Motion GUI-Bereich
	/// </summary>
	private void drawMotionArea()
	{
		drawMotionScrollArea();
		drawMotionManageArea();
	}
	
	/// <summary>
	/// Zeichnet die Scrollliste mit den Motions
	/// </summary>
	private void drawMotionScrollArea()
	{
		Rect tmpScrollRect = Resize.ResizeRectOperatorScreen(motionScrollRect);
		Rect tmpMotionRect = Resize.ResizeRectOperatorScreen(motionRect);
		Rect innerScrollRect = new Rect(0,0,tmpScrollRect.width-20, AvatarMotion.Motions.Count*tmpMotionRect.height);
		
		motionScrollPos = GUI.BeginScrollView(tmpScrollRect, motionScrollPos, innerScrollRect);
		drawMotionButtons();
		GUI.EndScrollView();
	}
	
	/// <summary>
	/// Zeichnet den Verwaltungsbuttons der Motions
	/// </summary>
	private void drawMotionManageArea()
	{
		Rect tmpManageArea = Resize.ResizeRectOperatorScreen(motionManageArea);
		Rect tmpManageButton = Resize.ResizeRectOperatorScreen(motionManageButton);
		
		GUI.enabled = selectedMotion != null && !renaming;
		GUI.BeginGroup(tmpManageArea);
		
		if(!player.IsPlaying()){
			if(GUI.Button(tmpManageButton, "Play")){
				player.PlayMotion(selectedMotion, true);
			}
		}
		else{
			if(GUI.Button(tmpManageButton, "Stop")){
				player.StopPlaying();
			}
		}
		tmpManageButton.x += tmpManageButton.width;
		
		if(GUI.Button(tmpManageButton, "Remove")){
			int motionIndex = AvatarMotion.Motions.IndexOf(selectedMotion);
			System.IO.File.Delete(selectedMotion.getAbsFilePath());
			AvatarMotion.Motions.Remove(selectedMotion);
			
			motionIndex = motionIndex >= AvatarMotion.Motions.Count ? motionIndex - 1 : motionIndex;
			
			if(motionIndex > -1)
				selectedMotion = AvatarMotion.Motions[motionIndex];
			else
				selectedMotion = null;
		}
		tmpManageButton.x += tmpManageButton.width;
		
		if(GUI.Button(tmpManageButton, "Rename")){
			renaming = true;
			renameString = selectedMotion.getName();
		}
		tmpManageButton.x += tmpManageButton.width;
		
		GUI.EndGroup();
		GUI.enabled = true;
	}
	
	/// <summary>
	/// Zeichnet die Motionbuttons
	/// </summary>
	private void drawMotionButtons()
	{	
		Rect tmpRect = Resize.ResizeRectOperatorScreen(motionRect);
	    foreach (AvatarMotion motion in AvatarMotion.Motions)
		{
			
		    if(GUI.Button(tmpRect, motion.getName(), selectedMotion == motion ? selButtonStyle : normalButtonStyle)) {
				if(selectedMotion == motion){
					selectedMotion = null;
					player.StopPlaying();
				}
				else{
					selectedMotion = motion;
					player.PlayMotion(motion, true);
				}
			}
			tmpRect.y += tmpRect.height;
	    }
	}
	
	/// <summary>
	/// Zeichnet das Umbenennen Popup
	/// </summary>
	private void drawRenamePopUp()
	{
		Rect tmpRenameArea = Resize.ResizeRectOperatorScreen(renameArea);
		Rect tmpTextRect = Resize.ResizeRectOperatorScreen(renameTextRect);
		Rect tmpOKRect = Resize.ResizeRectOperatorScreen(renameOKRect);
		Rect tmpCancelRect = Resize.ResizeRectOperatorScreen(renameCancelRect);
		
		GUI.Box(tmpRenameArea, "Rename Motion");
		GUI.BeginGroup(tmpRenameArea);
		renameString = GUI.TextField(tmpTextRect, renameString, textFieldStyle);
		
		if(GUI.Button(tmpOKRect, "OK"))
		{
			selectedMotion.setName(renameString);
			selectedMotion.writeToFile(recorder.motionDirectory+selectedMotion.getName()+" - "+selectedMotion.getTimestamp()+".mot");
			renameString = "";
			renaming = false;
		}
		
		if(GUI.Button(tmpCancelRect, "Cancel"))
		{
			renameString = "";
			renaming = false;
		}
		
		GUI.EndGroup();
	}
}
