﻿using UnityEngine;
using System.Collections;
/// <summary>
/// GUI main menu
/// Script zur Darstellung der "Menü-Szene"
/// Wird beim Start der Anwendung aufgerufen.
/// </summary>
public class GUIMainMenu : MonoBehaviour {
	
	private Rect buttonStartRec = new Rect(100,100,400,100);
	private Rect buttonStartOperator = new Rect(524,100,400,100);
	private Rect buttonQuit;
	
	/// <summary>
	///  Wird beim Laden des zugehörigen GameObjects automatisch einmal aufgerufen
	///	 Lädt zuerst alle Motions und stellt dann das Menü dar
	/// </summary>
	void Start () {
		//Startet den Motion Loader unter Script/Controller/MotionLoader.cs
		MotionLoader.Instance.StartLoading();
		calcRects();
	}
	
	/// <summary>
	/// Berechnet die Größe der drei Menü Buttons
	/// </summary>
	private void calcRects()
	{
		buttonStartRec = new Rect(Resize.ORIG_SCREENWIDTH/2 - Resize.ORIG_SCREENWIDTH/8, 100, Resize.ORIG_SCREENWIDTH/4, 100);
		buttonStartOperator = new Rect(buttonStartRec.xMin, buttonStartRec.yMax + buttonStartRec.height/2, buttonStartRec.width, buttonStartRec.height);
		buttonQuit = new Rect(buttonStartOperator.xMin, buttonStartOperator.yMax + buttonStartOperator.height/2, buttonStartOperator.width, buttonStartOperator.height);
	}
	
	/// <summary>
	/// Zuständig für das Zeichnen von GUI-Elementen
	/// Wird mindestens einmal pro Frame aufgerufen
	/// Zeichnet die drei Menü Buttons, wenn alle Motions geladen wurden
	/// </summary>
	void OnGUI () {		
		if(MotionLoader.Instance.LoadingDone())
		{		
			Rect tmpRect = Resize.ResizeRectOperatorScreen(buttonStartRec);
		    if(GUI.Button(tmpRect, "Recording Mode")) {
					Application.LoadLevel("Recording");
			}
			
			tmpRect = Resize.ResizeRectOperatorScreen(buttonStartOperator);
			if(GUI.Button(tmpRect, "Operating Mode")) {
					Application.LoadLevel("Operator");
			}
			
			tmpRect = Resize.ResizeRectOperatorScreen(buttonQuit);
			if(GUI.Button(tmpRect, "Quit Application")) {
					Application.Quit();
			}
		}
		
	}

}
