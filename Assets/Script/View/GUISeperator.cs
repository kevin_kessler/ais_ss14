﻿using UnityEngine;
using System.Collections;
/// <summary>
/// GUI Seperator
/// Script für die Unterteilung der Anzeige von Operator bzw. Record-Bereich und Customer-Bereich
/// Visuelle Eingrenzung des Operator bereiches bzw. des Recording bereiches
/// Legt eine transparente Ebene über den 4:3 Bildschirmbereich und lässt den 16:9 Bildschirmbereich frei
/// Somit kann ein GUI genau in der richtigen Größe entwickelt werden.
/// </summary>
public class GUISeperator : MonoBehaviour {
	
	
	float windowWidth = 0;
	float windowHeight = 0;
		
	/// <summary>
	/// Game Loop, wird einmal pro Frame aufgerufen - Übernimmt aktuelle breite und höhe vom Screen
	/// </summary>
	void Update () {
		windowHeight = Screen.height;
		windowWidth = Screen.width;
	}
	
	/// <summary>
	/// Zuständig für das Zeichnen von GUI-Elementen
	/// Wird mindestens einmal pro Frame aufgerufen
	/// Zeichnet die Ebene, welche die GUI in Operator bzw. Recorder Bereich und Customer-Bereich unterteilt
	/// </summary>
	void OnGUI()
	{
		GUI.Box(new Rect(0,0,windowWidth/28*12,windowHeight), "");
	}
}
