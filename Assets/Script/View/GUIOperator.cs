﻿using UnityEngine;
using System.Collections;
/// <summary>
/// GUI operator.
/// Script zur Darstellung der GUI-Elemente der "Operator-Szene".
/// Wird in dem GameObject "GUI" in der Operator-Szene verwendet
/// </summary>
public class GUIOperator : MonoBehaviour {
	//Public
	//------------------------
	public CustomerWebcam customerWebcam;
	public ShopAvatarAudioClient operatorAudioClient;
	public GameObject OperatorRobot;
	
	
	//Private
	//-----------------------
	private SensorOrPlayer sensorOrPlayer;
	private MotionPlayer player;	
	
	//Rechtecke für die Darstellung von Motions
	private Rect motionButtonRect;
	private Rect motionScrollRect;
	private Rect innerScrollRect;
	private Vector2 motionScrollPos;
	
	//Rechtecke zur Darstellung der unteren Navigationsleiste
	private Rect muteRect;
	private Rect selectCamRect;
	private Rect selectAudioRect;
	private Rect goIdleRect;
	private Rect switchModeRect;
	private Rect exitRect;
	
	private string [] webcams;
	
	//Button Style für den Mute Button
	private GUIStyle selButtonStyle;
	private bool styleSet = false;
	private bool b_muteMic;
	
	//Popup Data
	private bool b_chooseMicrophone;
	private bool b_chooseWebcam;
	private bool b_switch;
	private Rect popupBGRect;
	private Rect popupButtonRect;
		
	/// <summary>
	///  Wird beim Laden des zugehörigen GameObjects automatisch einmal aufgerufen
	///	 Instanziiert player, sensorOrPlayer und alle benötigten Rechtecke
	///	 Setzt alle Booleans auf false
	/// </summary>
	void Start () {
		player = KinectInstance.Instance.player;
		sensorOrPlayer = KinectInstance.Instance.sensorOrPlayer;
		calcRects();
		b_chooseWebcam = false;
		b_chooseMicrophone = false;
		b_switch = false;
		b_muteMic = false;
	}
	/// <summary>
	/// Zuständig für das Zeichnen von GUI-Elementen
	/// Wird mindestens einmal pro Frame aufgerufen
	/// Zeichnet alle in der Szene "Operator" verwendeten Buttons
	/// </summary>
	void OnGUI(){
		if(!styleSet){
			selButtonStyle = new GUIStyle(GUI.skin.button);
			selButtonStyle.normal.textColor = Color.red;
			selButtonStyle.hover.textColor = Color.red;
			styleSet = true;
		}
		
		drawMotionScrollArea();
		
		//Choose Cam Button
		Rect tmpSelectCamRect = Resize.ResizeRectOperatorScreen(selectCamRect);
		if(GUI.Button(tmpSelectCamRect, "Camera")){
			b_chooseWebcam = !b_chooseWebcam;
			b_switch = false;
		}
		
		//Mute/Unute Microphone	
		if(b_muteMic)
		{
			Rect tmpMuteRect = Resize.ResizeRectOperatorScreen(muteRect);
			if(GUI.Button(tmpMuteRect, "Unmute Mic", selButtonStyle)){
				//Unute Microphone	
				b_muteMic = !b_muteMic;
				operatorAudioClient.SetMicrophoneMute(false);
			}
		}else
		{
			Rect tmpMuteRect = Resize.ResizeRectOperatorScreen(muteRect);
			if(GUI.Button(tmpMuteRect, "Mute Mic")){
				//Mute Microphone	
				b_muteMic = !b_muteMic;
				operatorAudioClient.SetMicrophoneMute(true);
			}
		}
		
		//Choose Audio Button
		Rect tmpSelectAudioRect = Resize.ResizeRectOperatorScreen(selectAudioRect);
		if(GUI.Button(tmpSelectAudioRect, "Microphone")){
			b_chooseMicrophone = !b_chooseMicrophone;
			b_switch = true;
		}
		
		//Go Idle Button
		Rect tmpGoIdleRect = Resize.ResizeRectOperatorScreen(goIdleRect);
		if(GUI.Button(tmpGoIdleRect, "Go idle")){
			int rndIndex = Random.Range(0, AvatarMotion.IdleMotions.Count-1);
			player.PlayMotion(AvatarMotion.IdleMotions[rndIndex]);
		}
		
		//Switch Mode Button
		Rect tmpModeRect = Resize.ResizeRectOperatorScreen(switchModeRect);
		bool isPlayer = sensorOrPlayer.usingPlayer();
		if(GUI.Button(tmpModeRect, isPlayer ? "Go Live" : "Go Simulator")){
			if(isPlayer)
				sensorOrPlayer.useSensor();
			else
				sensorOrPlayer.usePlayer();
		}
		
		//Exit Button
		Rect tmpExitRect = Resize.ResizeRectOperatorScreen(exitRect);
		if(GUI.Button(tmpExitRect, "Exit")){
			customerWebcam.StopWebcam();
			Application.LoadLevel("MainMenu");
		}
		
		//Wechselt zwischen "Mikrofon wählen" und "Webcam wählen" PopUp-Fenster
		if(b_chooseMicrophone && b_chooseWebcam)
		{
			if(b_switch)
				b_chooseWebcam = !b_chooseWebcam;
			else
				b_chooseMicrophone = !b_chooseMicrophone;
		}
		
		//Zeichnet Pop-Up Fenster und blendet den Operator Roboter aus		
		if(b_chooseWebcam)
		{
			chooseWebcamPopUp();
			OperatorRobot.SetActive(false);
		}else if(b_chooseMicrophone)
		{
			chooseAudioPopUp();
			OperatorRobot.SetActive(false);
		}else{
			OperatorRobot.SetActive(true);
		}
	}
	
	/// <summary>
	/// Zeichnet die horizontale Scrollbox für die Motions
	/// </summary>
	private void drawMotionScrollArea()
	{
		Rect tmpScrollRect = Resize.ResizeRectOperatorScreen(motionScrollRect);
		Rect tmpInnerScrollRect = Resize.ResizeRectOperatorScreen(innerScrollRect);		
		GUI.Box(tmpScrollRect,"");
		motionScrollPos = GUI.BeginScrollView(tmpScrollRect, motionScrollPos, tmpInnerScrollRect);
		drawMotionButtons();
		GUI.EndScrollView();
	}

	/// <summary>
	/// Berechnet die Größen der benötigten Operator Buttons
	/// Nähere informationen hierzu in der Resize Klasse
	/// </summary>
	private void calcRects()
	{
		motionScrollRect = new Rect(30, 437, 964, 222);	
		Rect tmpScrollRect = Resize.ResizeRectOperatorScreen(motionScrollRect);	
		motionButtonRect = new Rect(0,0,135,42);		
		float rows = (Mathf.Ceil(AvatarMotion.Motions.Count / 4f));
		int breite = (int)((motionButtonRect.width * 1.2f * rows) + ((motionButtonRect.width * 1.05f) - motionButtonRect.width)) ;
		innerScrollRect = new Rect(0,0,breite, (int)(tmpScrollRect.height *1.1));
		motionScrollPos = Vector2.zero; 
		
		popupBGRect = new Rect (530,30, 390,200);
		popupButtonRect = new Rect (550,40, 350,42);
		
		switchModeRect = new Rect (30,Resize.ORIG_SCREENHEIGHT - 90, 140,70);
		goIdleRect = new Rect (193,Resize.ORIG_SCREENHEIGHT - 90, 140,70);
		muteRect = new Rect (356,Resize.ORIG_SCREENHEIGHT - 90, 140,70);
		selectAudioRect = new Rect (519,Resize.ORIG_SCREENHEIGHT - 90, 140,70);
		selectCamRect = new Rect (682,Resize.ORIG_SCREENHEIGHT - 90, 140,70);
		
		exitRect = new Rect(844, Resize.ORIG_SCREENHEIGHT - 90,150,70);
	}
		
	/// <summary>
	/// Zeichnet die Motion Buttons in die horizontale Scrollbar.
	/// </summary>
	private void drawMotionButtons()
	{	
		
		Rect tmpRect = new Rect(motionButtonRect);
		tmpRect = Resize.ResizeRectOperatorScreen(tmpRect);
		int tmp_counter = 0;
		foreach (AvatarMotion motion in AvatarMotion.Motions)
		{
			if(tmp_counter == 0)
			{
				tmpRect.y = (tmpRect.height * 1.2f) - tmpRect.height; 
				tmpRect.x += (tmpRect.width * 1.05f) - tmpRect.width;
			}
		    if(GUI.Button(tmpRect, motion.getName())) {
				player.PlayMotion(motion, false, true);
			}
			tmp_counter++;
			if(tmp_counter % 4 == 0)
			{
				tmpRect.y = (tmpRect.height * 1.2f) - tmpRect.height;
				tmpRect.x += tmpRect.width * 1.2f; 
			}else{
				tmpRect.y += tmpRect.height * 1.1f;
			}
	    }	    
	}
	
	/// <summary>
	/// Zeichnet das "WebCam Auswahl - PopUp Fenster" in dem der Benutzer eine WebCam als Standard-WebCam-Source festlegen kann
	/// </summary>
	private void chooseWebcamPopUp()
	{	
		popupBGRect.height = customerWebcam.getNumberOfWebcams() * 42 + 30;
		Rect tmpPopupBGRect = Resize.ResizeRectOperatorScreen(popupBGRect);
		
		GUI.Box(tmpPopupBGRect,"");
		Rect tmpPopupButtonRect;
		tmpPopupButtonRect = Resize.ResizeRectOperatorScreen(popupButtonRect);
		foreach(string webcamName in customerWebcam.GetDevices())
		{
			if(GUI.Button(tmpPopupButtonRect, webcamName)){
				customerWebcam.SetCustomerWebcam(webcamName);
				b_chooseWebcam = !b_chooseWebcam;
			}
			tmpPopupButtonRect.y += tmpPopupButtonRect.height * 1.1f;
		}
	}
	
	/// <summary>
	/// Zeichnet das "Mikrofon Auswahl - PopUp Fenster" in dem der Benutzer ein Mikrofon als Standard-Mikrofon-Source festlegen kann
	/// </summary>
	private void chooseAudioPopUp()
	{	
		
		popupBGRect.height = operatorAudioClient.getNumberOfMicrophones() * 42 + 30;
		Rect tmpPopupBGRect = Resize.ResizeRectOperatorScreen(popupBGRect);
		
		GUI.Box(tmpPopupBGRect,"");
		Rect tmpPopupButtonRect;
		tmpPopupButtonRect = Resize.ResizeRectOperatorScreen(popupButtonRect);
		foreach(string oneAudioSource in operatorAudioClient.GetDevices())
		{
			if(GUI.Button(tmpPopupButtonRect, oneAudioSource)){
				operatorAudioClient.SetMicrophone(oneAudioSource);
				b_chooseMicrophone = !b_chooseMicrophone;
			}
			tmpPopupButtonRect.y += tmpPopupButtonRect.height * 1.1f;
		}		
	}
}
