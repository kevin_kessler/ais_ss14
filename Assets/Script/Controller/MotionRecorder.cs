﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using Kinect;

/// <summary>
/// Diese Komponente des Kinect-GameObjects enthält Methoden zum Aufzeichnen der Motions.
/// Sie wird im RecordingModus benutzt um Aufzeichnungen zu Starten, zu Stoppen und die Aufzeichnung in einem AvatarMotion Object (und File) zu speichern.
/// </summary>
public class MotionRecorder : MonoBehaviour {
	
	/// <summary>
	/// Referenz zur SensorOrPlayer Komponente des Kinect-GameObjects.
	/// Wird benötigt um der SensorOrPlayer-Komponente mitteilen zu können wann zwischen MotionPlayer und KinectSensor umgeschaltet werden soll.
	/// Muss im Unity Inspector gesetzt werden.
	/// </summary>
	public SensorOrPlayer sensorOrPlayer;
	
	/// <summary>
	/// Der Absolute Dateipfad zum MotionDirectory.
	/// </summary>
	public string motionDirectory;
	
	private KinectInterface kinect;
	private AvatarMotion currentMotion;
	private bool isRecording;
	
	
	/// <summary>
	/// Wird beim Laden des zugehörigen GameObjects automatisch ein Mal aufgerufen.
	/// Dient zur Initialisierung der Klasse und ihrer Attribute.
	/// </summary>
	void Start () {
		kinect = sensorOrPlayer.getSensor();
		motionDirectory = Application.dataPath+@"/Motions/";
		
		if(!Directory.Exists(motionDirectory))
			Directory.CreateDirectory(motionDirectory);
			
		currentMotion = null;
		isRecording = false;
	}
	
	/// <summary>
	/// GameLoop, wird ein Mal pro Frame aufgerufen.
	/// Nimmt die Tastatureingabe zum starten und stoppen einer Aufzeichnung entgegen (Taste R).
	/// Fügt während einer Aufzeichnung die Skelettdaten eines jeden Frames zum aktuellen AvatarMotion Objekt hinzu.
	/// </summary>
	void Update () {
		
		if(!isRecording){
			if(Input.GetKeyUp(KeyCode.R)){
				StartRecording();
			}
		} else {
			if(Input.GetKeyUp(KeyCode.R)){
				StopRecording();
			}
		}
		
		//record skeletondata once per frame
		if(isRecording && null != currentMotion)
			if (kinect.pollSkeleton())
				currentMotion.addMotionFrame(new SerialSkeletonFrame(kinect.getSkeleton()));
	}
	
	/// <summary>
	/// Startet die Aufzeichnung einer Motion.
	/// Teilt dazu der SensorOrPlayer Instanz mit dass zu KinectSensor gewechselt werden soll und erstellt eine neue Motion die in der UpdateFunktion befüllt wird.
	/// Setzt die Recording Kontrollvariable auf true.
	/// </summary>
	public void StartRecording() {
		if(!isRecording){
			sensorOrPlayer.useSensor();
			isRecording = true;
			currentMotion = new AvatarMotion();
			Debug.Log("start recording");
		}
	}
	
	/// <summary>
	/// Beendet die aktuell laufende Motion Aufzeichnung.
	/// Veranlasst das serialisieren der aufgezeichneten Motion als .mot Datei und fügt die Motion der statischen AvatarMotion Liste hinzu.
	/// </summary>
	public void StopRecording() {
		if(isRecording){
			isRecording = false;
	
			string filePath = motionDirectory+currentMotion.getName()+" - "+currentMotion.getTimestamp()+".mot";
			currentMotion.writeToFile(filePath);
			AvatarMotion.Motions.Add(currentMotion);
			
			currentMotion = null;
			Debug.Log("stop recording");
		}
	}
	
	/// <summary>
	/// Überprüft ob gerade eine Aufzeichnung stattfindet oder nicht.
	/// </summary>
	/// <returns>
	/// <c>true</c> wenn ja; andererseits, <c>false</c>.
	/// </returns>
	public bool IsRecording(){
		return isRecording;
	}
}
