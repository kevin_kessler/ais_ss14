﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Diese Komponente des Kinect-GameObjects dient zum dynamischen Umschalten zwischen MotionPlayer (also dem KinectSimulator) und dem realen KinectSensor.
/// Das Umschalten bewirkt, dass die Kinect-GameObject Komponenten die Skelettdaten vom aktuell aktiven KinectInterface (MotionPlayer oder KinectSensor) beziehen.
/// </summary>
public class SensorOrPlayer : MonoBehaviour {
	
	/// <summary>
	/// Referenz zur KinectSensor Komponente des Kinect-GameObjects.
	/// </summary>
	public KinectSensor sensor;
	
	/// <summary>
	/// Referenz zur MotionPlayer Komponente des Kinect-GameObjects.
	/// </summary>
	public MotionPlayer player;
	
	private bool isPlayer = false;
	private SkeletonWrapper sw;
	
	/// <summary>
	/// Wird beim Laden des zugehörigen GameObjects automatisch ein Mal aufgerufen.
	/// Dient zur Initialisierung der Klasse und ihrer Attribute.
	/// Sorgt dafür dass das zugehörige GameObject beim Laden einer anderen Szene nicht zerstört wird und durchgehend weiter besteht.
	/// Dies stellt einen allgegenwärtigen globalen Zugriff auf dieses GameObjekt sicher.
	/// </summary>
	void Start () {
		DontDestroyOnLoad(this.gameObject);
		sw = (SkeletonWrapper)this.GetComponent("SkeletonWrapper");
	}
	
	/// <summary>
	/// Prüft ob aktuell der MotionPlayer oder der KinectSensor verwendet wird.
	/// </summary>
	/// <returns>
	/// True, wenn Player aktiv ist. False, wenn KinectSensor aktiv ist.
	/// </returns>
	public bool usingPlayer()
	{
		return isPlayer;
	}
	
	/// <summary>
	/// Teilt den Kinect-GameObject-Komponenten mit nun ihre Daten vom MotionPlayer zu beziehen.
	/// Dazu müssen die Komponenten die Methode getKinect() aufrufen.
	/// </summary>
	public void usePlayer()
	{
		if(!isPlayer){
			isPlayer = true;
			sw.Restart();
			//Debug.Log("Switched to player");
		}
	}
	
	/// <summary>
	/// Teilt den Kinect-GameObject-Komponenten mit nun ihre Daten vom KinectSensor zu beziehen.
	/// Dazu müssen die Komponenten die Methode getKinect() aufrufen.
	/// </summary>
	public void useSensor()
	{
		if(isPlayer){
			isPlayer = false;
			sw.Restart();
			//Debug.Log("Switched to sensor");
		}
	}
	
	/// <summary>
	/// Liefert die Referenz zur aktuell genutzten Kinect (KinectSimulator bzw. MotionPlayer oder realer KinectSensor) zurück.
	/// </summary>
	/// <returns>
	/// Die Referenz zur aktuell genutzen Kinect (Player oder Sensor).
	/// </returns>
	public Kinect.KinectInterface getKinect() {
		if(isPlayer){
			return player;
		}
		return sensor;
	}
	
	/// <summary>
	/// Liefert Referenz zum realen KinectSensor.
	/// </summary>
	/// <returns>
	/// Die Referenz zum KinectSensor.
	/// </returns>
	public Kinect.KinectInterface getSensor()
	{
		return sensor;
	}
	
	/// <summary>
	/// Liefert Referenz zum KinectSimulator, also dem MotionPlayer.
	/// </summary>
	/// <returns>
	/// Die Referenz zum KinectSimulator, also dem MotionPlayer.
	/// </returns>
	public Kinect.KinectInterface getPlayer()
	{
		return player;
	}
	
	
}
