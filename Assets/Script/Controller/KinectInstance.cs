﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Diese Klasse dient dem globalen Zugriff auf das Kinect GameObject.
/// Das Skript wird im Hauptmenü an das "KinectInitializer" GameObject angehängt.
/// Beim Laden des Objekts wird das Kinect GameObject mit all seinen Komponenten (Prefab) instantiiert.
/// Die Komponenten werden von den Attributen dieser Klasse Zwecks globalem Zugriff referenziert.
/// </summary>
public class KinectInstance : MonoBehaviour {
	
	/// <summary>
	/// Globale Instanz der Klasse, die den globalen Zugriff auf die Komponenten ermöglicht.
	/// </summary>
	public static KinectInstance Instance;
	
	/// <summary>
	/// Referenz zur SensorOrPlayer Komponente des Kinect GameObjects
	/// </summary>
	public SensorOrPlayer sensorOrPlayer;
	
	/// <summary>
	/// Referenz zur Sensor-Komponente des Kinect GameObjects
	/// </summary>
	public KinectSensor sensor;
	
	/// <summary>
	/// Referenz zur Player-Komponente des Kinect GameObjects
	/// </summary>
	public MotionPlayer player;
	
	/// <summary>
	/// Referenz zur Recorder-Komponente des Kinect GameObjects
	/// </summary>
	public MotionRecorder recorder;
	
	/// <summary>
	/// Referenz zur SkeletonWrapper-Komponente des Kinect GameObjects
	/// </summary>
	public SkeletonWrapper sw;
	
	/// <summary>
	/// Referenz zur DepthWrapper-Komponente des Kinect GameObjects
	/// </summary>
	public DepthWrapper dw;

	private static bool inited = false;
	private GameObject KinectPrefab;
	
	/// <summary>
	/// Wird beim Laden des zugehörigen GameObjects automatisch ein Mal aufgerufen.
	/// Wird zum automatischen Initialisieren des Kinect-GameObjects benutzt.
	/// Stellt sicher, dass das Kinect-GameObject nur ein mal initialisiert wird.
	/// </summary>
	void Start () 
	{
		if(!inited)
		{
			Init();
			Instance = this;
			DontDestroyOnLoad(this.gameObject);
		}
		else
			Destroy(this.gameObject);
	}
	
	/// <summary>
	/// Initialisiert das Kinect-GameObject, lädt seine Komponenten und weißt sie den Attributen der Klasse zu.
	/// </summary>
	private void Init()
	{
		if(!inited)
		{
			inited = true;
			KinectPrefab = (GameObject)Instantiate(Resources.Load("Prefabs/Kinect"));
			KinectPrefab.name = "Kinect";
			
			sensorOrPlayer = (SensorOrPlayer)KinectPrefab.GetComponent("SensorOrPlayer");
			sensor = (KinectSensor)KinectPrefab.GetComponent("KinectSensor");
			player = (MotionPlayer)KinectPrefab.GetComponent("MotionPlayer");
			recorder = (MotionRecorder)KinectPrefab.GetComponent("MotionRecorder");
			sw = (SkeletonWrapper)KinectPrefab.GetComponent("SkeletonWrapper");
			dw = (DepthWrapper)KinectPrefab.GetComponent("DepthWrapper");
		}
	}
	
	/// <summary>
	/// Setzt den Winkel des Kinectsensors auf den gegebenen Wert.
	/// </summary>
	/// <param name='angle'>
	/// Der Winkel der gesetzt werden soll. Max = 27, Min = -27
	/// </param>
	public void setSensorAngle(int angle)
	{
		sensor.setAngle(angle);
	}
}
