﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

/// <summary>
/// Diese Klasse sorgt dafür, dass beim Starten der Anwendung alle exisitierenden Motions und Idle-Motions geladen werden.
/// Dementsprechen ist die Klasse auch für das Anzeigen des Ladebalkens zuständig.
/// Die Motions werden aus den serialisierten Dateien aus dem MotionDirectory geladen und in AvatarMotion Objekte umgewandelt.
/// Die Klasse wird bei der Initialisierung des Skripts "GUIMainMenu" instantiiert um das automatische Laden bei Anwendungsstart zu aktivieren.
/// </summary>
public class MotionLoader : Singleton<MotionLoader> {
	
	private string motionDirectory;
	private bool loadingComplete = false;
	private bool isLoading = false;
	private bool isLoadingMotions = false;
	private bool isLoadingIdleMotions = false;
	private int numOfMotionFiles = 0;
	private int numOfIdleFiles = 0;
	
	private Rect barTextRect;
	private Rect outerBar;
	private Rect screenRect;
	private Texture2D outerBarTexture;
	private Texture2D innerBarTexture;
	private Texture2D blackTexture;
	private GUIStyle barTextStyle;

	/// <summary>
	/// Wird beim Laden des zugehörigen GameObjects automatisch ein Mal aufgerufen.
	/// Dient zur Initialisierung der Klasse und ihrer Attribute.
	/// Lädt benötigte Texturen um den Fortschrittsbalken darstellen zu können.
	/// Triggert das Laden der Motions.
	/// </summary>
	void Start () {
		motionDirectory = Application.dataPath+@"/Motions/";
		
		outerBarTexture = (Texture2D)Resources.Load("progressbarempty");
		innerBarTexture = (Texture2D)Resources.Load("progressbarfull");
		blackTexture = (Texture2D)Resources.Load("black");
		
		StartLoading();
		
		calcRects();
	}
	
	/// <summary>
	/// Berechnet die Positionen und Größen der Rechtecke die zum Zeichnen der Fortschrittsbalken benötigt werden.
	/// </summary>
	private void calcRects()
	{
		float barHeight = 70;
		float barWidth = Resize.ORIG_SCREENWIDTH/ 2;
		outerBar = new Rect(Resize.ORIG_SCREENWIDTH /2 - barWidth/2, Resize.ORIG_SCREENHEIGHT /2 - barHeight/2, barWidth, barHeight);
		barTextRect = new Rect(outerBar.xMin, outerBar.yMin - 60, outerBar.width, 70);
		
		screenRect = new Rect(0,0,Screen.width,Screen.height);
	}
	
	/// <summary>
	/// Zuständig für das Zeichnen von GUI-Elementen. Wird mindestens ein Mal pro Frame aufgerufen.
	/// Zeichnet den Bildschirm schwarz und ruft die Methode zum Zeichnen der Fortschrittsbalken auf.
	/// </summary>
	void OnGUI()
	{	
		if(null == barTextStyle){
			barTextStyle = new GUIStyle(GUI.skin.box);
			barTextStyle.normal.background = null;
		}
		
		if(isLoading)
		{
			GUI.DrawTexture(screenRect, blackTexture);
			drawProgressBar();
		}
	}
	
	/// <summary>
	/// Überprüft ob das Laden der Motions abgeschlossen ist.
	/// </summary>
	/// <returns>
	/// True wenn das Laden abgeschlossen ist. False andererseits.
	/// </returns>
	public bool LoadingDone()
	{
		return loadingComplete;
	}
	
	/// <summary>
	/// Startet die asynchrone Abarbeitung (Coroutine) zum Laden der Motions.
	/// </summary>
	public void StartLoading(){
		if(!loadingComplete && !isLoading){
			isLoading = true;
			StartCoroutine(LoadMotions());
		}
	}
	
	/// <summary>
	/// Coroutine zum asynchronen Laden der Motion-Files aus dem MotionDirectory.
	/// Lädt .mot und .idle Dateien, wandelt diese in AvatarMotion Objekte um und fügt sie den statischen AvatarMotion Listen hinzu.
	/// </summary>
	/// <returns>
	/// Nach jeder datei wird null geyielded um Unity zu sagen, dass ein Frame weitergesprungen werden soll bevor die nächste Datei geladen wird.
	/// Dies ist nötig damit der Fortschrittsbalken weiterhin gezeichnet werden kann.
	/// </returns>
	private IEnumerator LoadMotions()
	{
		motionDirectory = Application.dataPath+@"/Motions/";
		AvatarMotion.Motions.Clear();
		
		if(Directory.Exists(motionDirectory))
		{
			//load motions
			isLoadingMotions = true;
			string[] motionPaths = Directory.GetFiles(motionDirectory, "*.mot");
			numOfMotionFiles = motionPaths.Length;
			foreach(string path in motionPaths){
				AvatarMotion.Motions.Add(LoadMotionFromFile(path));
				yield return null;
			}
			isLoadingMotions = false;
			
			//load idlemotions
			isLoadingIdleMotions = true;
			string[] idlePaths = Directory.GetFiles(motionDirectory, "*.idle");
			numOfIdleFiles = idlePaths.Length;
			foreach(string path in idlePaths){
				AvatarMotion.IdleMotions.Add(LoadMotionFromFile(path));
				yield return null;
			}
			isLoadingIdleMotions = false;
		}
		
		loadingComplete = true;
		isLoading = false;
	}
	
	/// <summary>
	/// Lädt eine .mot oder .idle Datei vom angegebenen absoluten Dateipfad und wandelt diese in ein AvatarMotion Objekt um.
	/// </summary>
	/// <returns>
	/// Das geladene AvatarMotion Objekt
	/// </returns>
	/// <param name='filePath'>
	/// Der absolute Dateipfad zur MotionFile
	/// </param>
	private AvatarMotion LoadMotionFromFile(string filePath)
	{
		FileStream file = new FileStream(@filePath, FileMode.Open);
		BinaryFormatter bf = new BinaryFormatter();
		AvatarMotion motion = (AvatarMotion)bf.Deserialize(file);
		motion.setAbsFilepath(filePath); //set filepath to update eventually copied files
		
		file.Close();
		
		return motion;
	}
	
	/// <summary>
	/// Zuständig für das Zeichnen der Fortschrittsbalken.
	/// Wird aus OnGUI() heraus aufgerufen.
	/// </summary>
	private void drawProgressBar()
	{
		float progress = 0;
		string loadingText = "Loading...";
		if(isLoadingMotions){
			progress = (float)AvatarMotion.Motions.Count/numOfMotionFiles;
			loadingText = "Loading Captured Motions: "+AvatarMotion.Motions.Count+"/"+numOfMotionFiles;
		}
		else if(isLoadingIdleMotions){
			progress = (float)AvatarMotion.IdleMotions.Count/numOfIdleFiles;
			loadingText = "Loading Idle Motions: "+AvatarMotion.IdleMotions.Count+"/"+numOfIdleFiles;
		}
		
		Rect tmpOuter = Resize.ResizeRectOperatorScreen(outerBar);
		int innerMargin = 10;
		float innerMaxLength = tmpOuter.width-innerMargin;
		Rect innerBar = new Rect(innerMargin/2,innerMargin/2, innerMaxLength*progress, tmpOuter.height - innerMargin);
		
		Rect tmpTextRect = Resize.ResizeRectOperatorScreen(barTextRect);
		
		GUI.Box(tmpTextRect, loadingText, barTextStyle);
		//GUI.Box(tmpTextRect, motionDirectory);
		GUI.DrawTexture(tmpOuter, outerBarTexture);
		GUI.BeginGroup(tmpOuter);
		GUI.DrawTexture(innerBar, innerBarTexture);
		GUI.EndGroup();
	}
}
