﻿using UnityEngine;
using System.Collections;
using System.Collections.ObjectModel;
using Kinect;

/// <summary>
/// Diese Klasse simuliert einen KinectSensor und wird zum Abspielen der aufgezeichneten Motions verwendet.
/// Sie implementiert die Schnittstelle "KinectInterface", damit zwischen ihr und der Klasse "KinectSensor" dynamisch umgeschaltet werden kann.
/// Die Komponenten des Kinect-GameObjects (SkeletonWrapper z.B.) bedienen sich entweder dieser oder der KinectSensor Instanz um Skelett-Daten zu pollen.
/// Der MotionPlayer lädt dabei die Skelett-Daten aus den AvatarMotion Objekten und stellt sie dem SkeletonWrapper zum pollen bereit.
/// (Im Gegensatz zur KinectSensor-Klasse, welche dem SkeletonWrapper Echtzeit-Daten zum pollen bereit stellt).
/// </summary>
public class MotionPlayer : MonoBehaviour, KinectInterface {
	
	/// <summary>
	/// Referenz zur SensorOrPlayer Komponente des Kinect-GameObjects.
	/// Wird benötigt um der SensorOrPlayer-Komponente mitteilen zu können wann zwischen MotionPlayer und KinectSensor umgeschaltet werden soll.
	/// Muss im Unity Inspector gesetzt werden.
	/// </summary>
	public SensorOrPlayer sensorOrPlayer;

	private bool isPlaying = false;
	private AvatarMotion currentMotion = null;
	private bool newSkeleton = false;
	private int frameSincePlaying = 0;
	private float timeSincePlaying = 0;
	private float playbackSpeed = 0.0333f; //the higher, the slower it plays
	private bool switchToSensorOnStop = false;
	private bool goIdleOnStop = false;
	
	/// <summary>
	/// Höhe des simulierten Kinectsensors (In Metern vom Boden hinweg). 
	/// Sollte dem Wert entsprechen mit dem die Motions vom realen KinectSensor aufgezeichnet wurden.
	/// </summary>
	public float sensorHeight;
	
	/// <summary>
	/// Wo soll der Kinectsimulator seinen Nullpunkt des Koordinatensystems registrieren? (Relativ zum Boden direkt unter dem Sensor)
	/// Sollte dem Wert entsprechen mit dem die Motions vom realen KinectSensor aufgezeichnet wurden.
	/// </summary>
	public Vector3 kinectCenter;
	
	/// <summary>
	/// Punkt zu dem der Kinectsimulator hinsehen soll. (Relativ zu kinectCenter)
	/// Sollte dem Wert entsprechen mit dem die Motions vom realen KinectSensor aufgezeichnet wurden.
	/// </summary>
	public Vector4 lookAt;

	/// <summary>
	/// GameLoop, wird ein Mal pro Frame aufgerufen.
	/// Dient dazu den PlayTimer hochzuzählen und überprüft ob die aktuell spielende Motion zu Ende ist (ruft dann entsprechend StopPlaying() auf).
	/// </summary>
	void Update () 
	{
		if(isPlaying && null != currentMotion){
			timeSincePlaying += Time.deltaTime;
			
			//stop playing when last frame of motion is reached
			if(frameSincePlaying >= currentMotion.getMotionFrames().Count)
				StopPlaying();
		}
	}
	
	
	/// <summary>
	/// Wird wie Update() ein Mal pro Frame aufgerufen, allerdings immer nach Update().
	/// Setzt die Kontrollvariable zum pollen von Skelettdaten auf false, damit in falschen Frames nicht gepollt wird.
	/// </summary>
	void LateUpdate () {
		newSkeleton = false;
	}
	
	/// <summary>
	/// Überprüft ob der Kinectsimulator gerade eine Motion abspielt oder nicht.
	/// </summary>
	/// <returns>
	/// <c>true</c> wenn eine Motion abgespielt wird; andererseits, <c>false</c>.
	/// </returns>
	public bool IsPlaying()
	{
		return isPlaying;
	}
	
	/// <summary>
	/// Bereitet das Abspielen der übergebenen Motion vor und stellt die Skelettdaten dieser Motion zum pollen bereit, wodurch diese vom Skelettonwrapper abgespielt werden kann.
	/// Aktiviert den Simulator, indem es der SensorOrPlayer Instanz mitteilt auf Player zu wechseln.
	/// </summary>
	/// <param name='motion'>
	/// Die Motion die abgespielt werden soll.
	/// </param>
	/// <param name='switchToSensorAfterPlaying'>
	/// Wenn true, wird nach dem Abspielen der Motion die Simulation beendet und zum Sensor zurück gewechselt.
	/// </param>
	/// <param name='goIdleAfterPlaying'>
	/// Wenn true, wird nach dem Abspielen der Motion eine zufällige Idle-Motion gestartet. (Triggert nur wenn switchToSensorAfterPlaying == false)
	/// </param>
	public void PlayMotion(AvatarMotion motion, bool switchToSensorAfterPlaying = false, bool goIdleAfterPlaying = false)
	{
		sensorOrPlayer.usePlayer();
		currentMotion = motion;
		isPlaying = true;
		frameSincePlaying = 0;
		timeSincePlaying = 0;
		
		switchToSensorOnStop = switchToSensorAfterPlaying;
		goIdleOnStop = goIdleAfterPlaying;
		Debug.Log("Playing Motion: "+motion.getAbsFilePath());
	}
	
	/// <summary>
	/// Beendet das Abspielen der aktuell laufenden Motion. 
	/// Switcht je nach Werten der Kontrollvariablen in SensorMode oder IdleMode oder nichts von beidem.
	/// </summary>
	public void StopPlaying()
	{
		currentMotion = null;
		isPlaying = false;
		frameSincePlaying = 0;
		timeSincePlaying = 0;
		
		if(switchToSensorOnStop){
			sensorOrPlayer.useSensor();
			switchToSensorOnStop = false;
		}
		else if(goIdleOnStop){
			int rndIndex = Random.Range(0, AvatarMotion.IdleMotions.Count-1);
			PlayMotion(AvatarMotion.IdleMotions[rndIndex], false, true);
		}
	}
	
	float KinectInterface.getSensorHeight() {
		return sensorHeight;
	}
	Vector3 KinectInterface.getKinectCenter() {
		return kinectCenter;
	}
	Vector4 KinectInterface.getLookAt() {
		return lookAt;
	}
	
	/// <summary>
	/// Prüft ob ein neuer Frame erreicht und somit neue Skelettdaten zur verfügung stehen.
	/// Wird regelmäßig vom SkeletonWrapper aufgerufen.
	/// </summary>
	/// <returns>
	/// true, wenn neuer Frame erreicht; false andererseits
	/// </returns>
	bool KinectInterface.pollSkeleton() {
		int frame = Mathf.FloorToInt(timeSincePlaying / playbackSpeed);
		if(frame > frameSincePlaying){
			frameSincePlaying = frame;
			newSkeleton = true;
		}
		return newSkeleton;
	}
	
	/// <summary>
	/// Liefert die Skelettdaten des aktuellen Frames der Motion um diese auf den Avatar abbilden zu können.
	/// Wird regelmäßig vom SkeletonWrapper aufgerufen.
	/// </summary>
	/// <returns>
	/// Die Skelettdaten des aktuellen Frames der Motion.
	/// </returns>
	NuiSkeletonFrame KinectInterface.getSkeleton() {
		ReadOnlyCollection<SerialSkeletonFrame> frames = currentMotion.getMotionFrames();
		return frames[frameSincePlaying % frames.Count].deserialize(); //modulo to prevent out of bounds in any case
	}
	
	/// <summary>
	/// Berechnet die BoneOrientations der übergeben Skelettdaten und liefert diese zurück.
	/// </summary>
	/// <returns>
	/// Die berechneten BoneOrientations der übergebenen Skelettdaten.
	/// </returns>
	/// <param name='skeletonData'>
	/// Die Skelettdaten zu denen die BoneOrientations berechnet werden sollen.
	/// </param>
	NuiSkeletonBoneOrientation[] KinectInterface.getBoneOrientations(NuiSkeletonData skeletonData){
		NuiSkeletonBoneOrientation[] boneOrientations = new NuiSkeletonBoneOrientation[(int)(NuiSkeletonPositionIndex.Count)];
		NativeMethods.NuiSkeletonCalculateBoneOrientations(ref skeletonData, boneOrientations);
		return boneOrientations;
	}
	
	bool KinectInterface.pollColor() {
		return false;
	}
	
	Color32[] KinectInterface.getColor() {
		return null;
	}
	
	bool KinectInterface.pollDepth() {
		return false;
	}
	
	short[] KinectInterface.getDepth() {
		return null;
	}
}
