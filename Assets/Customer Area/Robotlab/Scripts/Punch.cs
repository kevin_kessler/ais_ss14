using UnityEngine;
using System.Collections;

public class Punch : MonoBehaviour
{
	private Animator anim;
	private int hitState;
	private int doPunchBool;
	private bool punching;
	private float punchingTimer;
	private const float punchingIntervall = 10;
	
	void Awake ()
	{
		punching = false;
		punchingTimer = 0;
		anim = GetComponent<Animator>();
		hitState = Animator.StringToHash("Base Layer.Hit");
		doPunchBool = Animator.StringToHash("DoPunch");
	}
	
	
	private void doPunch()
	{
		anim.SetBool(doPunchBool, true);
		punching = true;
		punchingTimer = 0;
	}
	
	void Update ()
	{
		punchingTimer += Time.deltaTime;
		
		if(anim.GetCurrentAnimatorStateInfo(0).nameHash == hitState){
			anim.SetBool(doPunchBool, false);
			punching = false;
		}
		
		if(!punching && punchingTimer > punchingIntervall)
			doPunch();
	}
}
